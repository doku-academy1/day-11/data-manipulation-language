SET search_path To alta_online_shop, public;
-- ALTER TABLE users
-- ALTER COLUMN gender TYPE VARCHAR(1);

-- INSERT INTO users
-- (status, tanggal_lahir, gender, created_at, updated_at)
-- VALUES (1, '2000-12-11', 'M', current_timestamp, current_timestamp);

DELETE FROM transaction_details
WHERE id between 12 and 56;

ALTER SEQUENCE transaction_details_id_seq RESTART WITH 1;
-- SELECT setval('users_id_seq', (SELECT max(id) FROM users));

-- Insert 5 Operators
INSERT INTO operators
(operator_name, created_at, updated_at)
VALUES('XL', now(), now());

INSERT INTO operators
(operator_name, created_at, updated_at)
VALUES('Simpati', now(), now());

INSERT INTO operators
(operator_name, created_at, updated_at)
VALUES('Mentari', now(), now());

INSERT INTO operators
(operator_name, created_at, updated_at)
VALUES('Axis', now(), now());

INSERT INTO operators
(operator_name, created_at, updated_at)
VALUES('Indosat', now(), now());


-- Insert 3 product types
INSERT INTO product_types
(name_product_type, created_at, updated_at)
VALUES('Prabayar', now(), now());

INSERT INTO product_types
(name_product_type, created_at, updated_at)
VALUES('Pascabayar', now(), now());

INSERT INTO product_types
(name_product_type, created_at, updated_at)
VALUES('Voucher', now(), now());


-- remove product desc id and add product id in table product descriptions
ALTER TABLE products
DROP COLUMN product_desc_id;

ALTER TABLE product_descriptions
ADD COLUMN product_id int not null;

ALTER TABLE product_descriptions
add constraint fk_product_id_to_product_desc
foreign key (product_id)
references products(id);


-- Insert 2 product dengan product_type_id = 1 dan operators_id = 3
INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(1, 3, 'CODE01', 'Kartu Prabayar Mentari 1', 1, now(), now());

INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(1, 3, 'CODE01', 'Kartu Prabayar Mentari 2', 1, now(), now());


-- Insert 3 product dengan product_type_id = 2 dan operators_id = 1
INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(2, 1, 'CODE02', 'Kartu Pascabayar XL 1', 1, now(), now());

INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(2, 1, 'CODE02', 'Kartu Pascabayar XL 2', 1, now(), now());

INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(2, 1, 'CODE02', 'Kartu Pascabayar XL 3', 1, now(), now());


-- Insert 3 product dengan product_type_id = 3 dan operators_id = 4
INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(3, 4, 'CODE03', 'Kartu Voucher Axis 1', 1, now(), now());

INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(3, 4, 'CODE03', 'Kartu Voucher Axis 2', 1, now(), now());

INSERT INTO products
(product_typeid, operator_id, code, name_product, status, created_at, updated_at)
VALUES(3, 4, 'CODE03', 'Kartu Voucher Axis 3', 1, now(), now());


-- Insert product descriptions setiap product
INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator mentari 10 ribu', now(), now(), 1);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator mentari 20 ribu', now(), now(), 2);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator XL 10 ribu', now(), now(), 3);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator XL 20 ribu', now(), now(), 4);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator XL 50 ribu', now(), now(), 5);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator Axis 10 ribu', now(), now(), 6);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator Axis 20 ribu', now(), now(), 7);

INSERT INTO product_descriptions
(description, created_at, updated_at, product_id)
VALUES('Kartu prabayar untuk operator Axis 50 ribu', now(), now(), 8);


-- Insert 3 payment methods
INSERT INTO payment_methods
(name_payment, created_at, updated_at)
VALUES('Bank Transfer', now(), now());

INSERT INTO payment_methods
(name_payment, created_at, updated_at)
VALUES('E-wallet', now(), now());

INSERT INTO payment_methods
(name_payment, created_at, updated_at)
VALUES('PayLater', now(), now());


-- Insert 5 users
INSERT INTO users
(status, tanggal_lahir, gender, created_at, updated_at)
VALUES(1, '2000-12-12', 'M', now(), now());

INSERT INTO users
(status, tanggal_lahir, gender, created_at, updated_at)
VALUES(1, '2000-11-11', 'F', now(), now());

INSERT INTO users
(status, tanggal_lahir, gender, created_at, updated_at)
VALUES(1, '2000-10-10', 'M', now(), now());

INSERT INTO users
(status, tanggal_lahir, gender, created_at, updated_at)
VALUES(1, '2000-09-09', 'F', now(), now());

INSERT INTO users
(status, tanggal_lahir, gender, created_at, updated_at)
VALUES(1, '2000-08-08', 'M', now(), now());


-- Insert 3 transactions per user
-- ALTER SEQUENCE transactions_id_seq RESTART WITH 1;
--  user 1
INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(1, 1, 'Pending', 3, 30000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(1, 2, 'Pending', 3, 30000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(1, 3, 'Pending', 3, 30000.00, now(), now());

--  user 2
INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(2, 1, 'Pending', 3, 60000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(2, 2, 'Pending', 3, 60000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(2, 3, 'Pending', 3, 60000.00, now(), now());

--  user 3
INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(3, 1, 'Pending', 3, 90000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(3, 2, 'Pending', 3, 90000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(3, 3, 'Pending', 3, 90000.00, now(), now());

--  user 4
INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(4, 1, 'Pending', 3, 60000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(4, 2, 'Pending', 3, 60000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(4, 3, 'Pending', 3, 60000.00, now(), now());

--  user 5
INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(5, 1, 'Pending', 3, 30000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(5, 2, 'Pending', 3, 30000.00, now(), now());

INSERT INTO transactions
(user_id, payment_method_id, status, total_qty, total_price, created_at, updated_at)
VALUES(5, 3, 'Pending', 3, 30000.00, now(), now());


-- Insert 3 product di masing-masing transaksi

-- transaction 1
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(1, 1, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(1, 2, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(1, 3, 'Pending', 1, 10000.00, now(), now());

-- transaction 2
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(2, 4, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(2, 5, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(2, 6, 'Pending', 1, 10000.00, now(), now());

-- transaction 3
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(3, 7, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(3, 8, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(3, 1, 'Pending', 1, 10000.00, now(), now());

-- transaction 4
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(4, 1, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(4, 2, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(4, 3, 'Pending', 1, 20000.00, now(), now());

-- transaction 5
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(5, 4, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(5, 5, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(5, 6, 'Pending', 1, 20000.00, now(), now());

-- transaction 6
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(6, 7, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(6, 8, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(6, 1, 'Pending', 1, 20000.00, now(), now());

-- transaction 7
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(7, 1, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(7, 2, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(7, 3, 'Pending', 1, 30000.00, now(), now());

-- transaction 8
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(8, 4, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(8, 5, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(8, 6, 'Pending', 1, 30000.00, now(), now());

-- transaction 9
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(9, 7, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(9, 8, 'Pending', 1, 30000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(9, 1, 'Pending', 1, 30000.00, now(), now());

-- transaction 10
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(10, 1, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(10, 2, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(10, 3, 'Pending', 1, 20000.00, now(), now());

-- transaction 11
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(11, 4, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(11, 5, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(11, 6, 'Pending', 1, 20000.00, now(), now());

-- transaction 12
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(12, 7, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(12, 8, 'Pending', 1, 20000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(12, 1, 'Pending', 1, 20000.00, now(), now());

-- transaction 13
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(13, 1, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(13, 2, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(13, 3, 'Pending', 1, 10000.00, now(), now());

-- transaction 14
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(14, 4, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(14, 5, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(14, 6, 'Pending', 1, 10000.00, now(), now());

-- transaction 15
INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(15, 7, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(15, 8, 'Pending', 1, 10000.00, now(), now());

INSERT INTO transaction_details
(transaction_id, product_id, status, qty, price, created_at, updated_at)
VALUES(15, 1, 'Pending', 1, 10000.00, now(), now());



-- SELECT --

-- nama user dengan jenis kelamin laki-laki (M)
SELECT * FROM users
WHERE gender = 'M';

-- product dengan id = 3;
SELECT * FROM products
WHERE id = 3;

-- data user yang created_at dalam 7 hari kebelakang dan mempunyai nama mengandung huruf 'a'
ALTER TABLE users
ADD COLUMN name varchar(20) not null;

UPDATE users SET
name = 'rizki'
WHERE id = 1;

UPDATE users SET
name = 'indra'
WHERE id = 2;

UPDATE users SET
name = 'rosyid'
WHERE id = 3;

UPDATE users SET
name = 'alif'
WHERE id = 4;

UPDATE users SET
name = 'aldi'
WHERE id = 5;

UPDATE users SET
created_at = '2022-10-09'
WHERE id = 1;

SELECT * FROM users
WHERE created_at > current_date - interval '7 days' AND name like '%a%';

-- hitung jumlah user perempuan (F)
SELECT COUNT(gender) as gender FROM users
WHERE gender = 'F';

-- data pelanggan sesuai abjad
SELECT name FROM users
ORDER BY name;

-- tampilkan 5 data pada product
SELECT * FROM products
LIMIT 5;



-- UPDATE --

-- update product id = 1 dengan nama product dummy 
UPDATE products SET
name_product = 'Product Dummy'
WHERE id = 1;

-- update qty = 3 pada transaction details where product id = 1
UPDATE transaction_details SET
qty = 3
WHERE product_id = 1;

SELECT * FROM products;



-- DELETE --

-- delete pada tabel product id = 1
ALTER TABLE transaction_details
DROP CONSTRAINT fk_product_id,
ADD CONSTRAINT fk_product_id
foreign key (product_id)
references products(id) on delete cascade;

ALTER TABLE product_descriptions
drop constraint fk_product_id_to_product_desc,
add constraint fk_product_id_to_product_desc
foreign key (product_id)
references products(id) on delete cascade;

DELETE FROM products
WHERE id = 1;

-- delete pada tabel product dimana product_type = 1
DELETE FROM products
WHERE product_typeid = 1;



-- Join, Union, Sub-Query, Fnction --

-- 1. Gabungkan data transaksi dari user id 1 dan user id 2
SELECT * FROM transactions
WHERE user_id in (1, 2);

-- 2. Tampilkan jumlah harga transaksi user id = 1
SELECT SUM(total_price) as jumlah_harga_transaksi FROM transactions
WHERE user_id = 1;

-- 3. Tampilkan total transaksi product type 2
SELECT count(id) as total_transaksi FROM transactions
WHERE id in (SELECT distinct(transaction_id) FROM transaction_details
				WHERE product_id in (SELECT id FROM products 
									 WHERE product_typeid=2)
			);

-- 4. Tampilkan semua field table product dan field name table product type
SELECT p.*, t.name_product_type FROM products p
INNER JOIN product_types t
ON p.product_typeid = t.id;

-- 5. Tampilkan semua field table transaction, field name table product dan field name table user
SELECT t.*, p.name_product, u.name FROM transactions t
INNER JOIN users u
	ON u.id = t.user_id
	INNER JOIN transaction_details as td
	ON td.transaction_id = t.id
		INNER JOIN products p
		ON p.id = td.product_id;

-- 6. Buat function setelah data transaksi dihapus maka transaction details juga dengan transaction id yang dimaksud
CREATE OR REPLACE FUNCTION delete_transaction() RETURNS trigger AS $$
BEGIN
	DELETE FROM transaction_details 
	WHERE transaction_id = OLD.id;
	RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER delete_transaction_tigger
BEFORE DELETE ON transactions FOR EACH ROW
EXECUTE FUNCTION delete_transaction();

-- 7.
CREATE OR REPLACE FUNCTION update_qty() RETURNS trigger AS $$
BEGIN
	UPDATE transactions
	SET total_qty = total_qty - OLD.total_qty
	WHERE id = OLD.transaction_id;
	RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER update_qty_transaction_tigger
BEFORE DELETE ON transaction_details FOR EACH ROW
EXECUTE FUNCTION update_qty();
        
-- 8. Data products yang tidak ada di tabel transaction_details
SELECT (product_id) as product_yang_tidak_terpakai FROM transaction_details
WHERE product_id not in (SELECT id FROM products);